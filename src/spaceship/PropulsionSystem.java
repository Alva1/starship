/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package spaceship;

/**
 *
 * @author Marist User
 */

//import java.util.Scanner;

public class PropulsionSystem {
  
    private int speed;
  
    public PropulsionSystem() {
        speed = 0;
    }
  
   // public int speed;
  
    public void increase(){
        speed += 100;
    }
  
    public void decrease() {
        if(speed >= 100) {
            speed -= 100;
        }
    }
  
    public int read(){
        return speed;
    }

}
