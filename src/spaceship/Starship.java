/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package spaceship;

/**
 *
 * @author Marist User
 */
import java.util.Scanner;

public class Starship {
  
    private PropulsionSystem propulsion;
     
    public Starship(){
        this.propulsion = new PropulsionSystem();
    }
  
    public static void main(String[] args) {
        // TODO code application logic
        Starship myShip = new Starship();
       
        Scanner userInput = new Scanner(System.in);
       
        String inputStr = "speed";
       
        while(inputStr.equals("quit") == false){

               myShip.inputCommand(inputStr);
           
               System.err.println("Enter accel or deccel to increase or decrease speed by 100 or speed to check current speed.");
               System.err.println("You can exit by entering quit.");
              
               inputStr = userInput.nextLine().toLowerCase();
        }
    }
  
    public void inputCommand(String cmd) {
      
        if(cmd.equals("accel")) {
            this.propulsion.increase();
        }
        else if (cmd.equals("deccel")) {
            this.propulsion.decrease();
        } else if(cmd.equals("speed")) {
            System.out.println("The current speed of the spaceship is " + this.propulsion.read());
        } else {
            System.err.println("Wrong input try again.");
        }
    }
}
